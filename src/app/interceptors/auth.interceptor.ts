import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthenticationService} from "../services/authentication.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthenticationService) {
  }

  intercept(httpRequest: HttpRequest<any>, httpHandler: HttpHandler): Observable<HttpEvent<any>> {
    const requestUrl = httpRequest.url;
    const host = this.authService.host;
    if (requestUrl.includes(`${host}/user/login`)
        || requestUrl.includes(`${host}/user/register`)) {
      return httpHandler.handle(httpRequest);
    }
    this.authService.loadTokenFromLocalStorage();
    const token = this.authService.getToken();
    const requestWithToken = httpRequest.clone({setHeaders: {Authorization: `Bearer ${token}`}});
    return httpHandler.handle(requestWithToken);
  }
}
