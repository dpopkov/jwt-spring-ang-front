import {Injectable} from '@angular/core';
import {NotifierService} from "angular-notifier";
import {NotificationType} from "../enums/notification-type";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private notifier: NotifierService) {
  }

  public notifyDefault(message: string) {
    this.notifier.notify(NotificationType.DEFAULT, message);
  }

  public notifySuccess(message: string) {
    this.notifier.notify(NotificationType.SUCCESS, message);
  }

  public notifyError(message: string) {
    this.notifier.notify(NotificationType.ERROR, message);
  }

  public notifyInfo(message: string) {
    this.notifier.notify(NotificationType.INFO, message);
  }

  public notifyWarning(message: string) {
    this.notifier.notify(NotificationType.WARNING, message);
  }

  private notify(type: NotificationType, message: string): void {
    this.notifier.notify(type, message);
  }
}
