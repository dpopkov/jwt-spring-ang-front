import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpErrorResponse, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../model/user";
import {JwtHelperService} from "@auth0/angular-jwt";
import {ServiceConstants} from "./service-constants";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public readonly host = environment.apiUrl;

  private token: string;
  private loggedInUsername: string;
  private jwtHelper = new JwtHelperService();

  constructor(private http: HttpClient) {
  }

  public register(user: User): Observable<User> {
    return this.http.post<User>(`${this.host}/user/register`, user);
  }

  public login(user: User): Observable<HttpResponse<User>> {
    return this.http.post<User>(`${this.host}/user/login`,
      user, {observe: 'response'}); // adding "observe:'response'" in order to get the whole response, not body
  }

  public logOut(): void {
    console.log('AuthenticationService: logging out.');
    this.token = null;
    this.loggedInUsername = null;
    localStorage.removeItem(ServiceConstants.USER_KEY);
    localStorage.removeItem(ServiceConstants.TOKEN_KEY);
    localStorage.removeItem(ServiceConstants.USERS_KEY);
  }

  public saveToken(token: string): void {
    this.token = token;
    localStorage.setItem(ServiceConstants.TOKEN_KEY, token);
  }

  public addUserToLocalCache(user: User): void {
    localStorage.setItem(ServiceConstants.USER_KEY, JSON.stringify(user));
  }

  public getUserFromLocalCache(): User {
    return JSON.parse(localStorage.getItem(ServiceConstants.USER_KEY));
  }

  public loadTokenFromLocalStorage(): void {
    this.token = localStorage.getItem(ServiceConstants.TOKEN_KEY);
  }

  public getToken(): string {
    return this.token;
  }

  public isLoggedIn(): boolean {
    this.loadTokenFromLocalStorage();
    if (this.token != null && this.token !== '') {
      let decodedToken = this.jwtHelper.decodeToken(this.token);
      let username = decodedToken.sub;
      if (username != null && username !== '') {
        if (!this.jwtHelper.isTokenExpired(this.token)) {
          this.loggedInUsername = username;
          return true;
        } else {
          console.log('AuthenticationService: token is expired');
          // todo: should logout?
          return false;
        }
      }
    } else {
      this.logOut();
      return false;
    }
    return false;
  }
}
