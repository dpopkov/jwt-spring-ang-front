import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpErrorResponse, HttpEvent} from "@angular/common/http";
import {User} from "../model/user";
import {Observable} from "rxjs";
import {CustomHttpResponse} from "../model/custom-http-response";
import {ServiceConstants} from "./service-constants";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly host = environment.apiUrl;
  private readonly usersUrl = `${this.host}/user`;

  constructor(private http: HttpClient) { }

  public getUsers(): Observable<User[] | HttpErrorResponse> {
    return this.http.get<User[]>(`${this.usersUrl}/list`);
  }

  public addUser(formData: FormData): Observable<User | HttpErrorResponse> {
    return this.http.post<User>(`${this.usersUrl}/add`, formData);
  }

  public updateUser(formData: FormData): Observable<User | HttpErrorResponse> {
    return this.http.put<User>(`${this.usersUrl}/update`, formData);
  }

  public resetPasswordFor(email: string): Observable<CustomHttpResponse | HttpErrorResponse> {
    return this.http.put<CustomHttpResponse>(`${this.usersUrl}/resetPassword/${email}`, '');
  }

  public updateProfileImage(formData: FormData): Observable<HttpEvent<User> | HttpErrorResponse> {
    return this.http.put<User>(`${this.usersUrl}/updateProfileImage`, formData,
      {reportProgress: true, observe: 'events'});
  }

  public deleteUserByUsername(username: string): Observable<CustomHttpResponse | HttpErrorResponse> {
    return this.http.delete<CustomHttpResponse>(`${this.usersUrl}/delete/${username}`);
  }

  public addUsersToLocalCache(users: User[]): void {
    localStorage.setItem(ServiceConstants.USERS_KEY, JSON.stringify(users));
  }

  public getUsersFromLocalCache(): User[] {
    const users = localStorage.getItem(ServiceConstants.USERS_KEY);
    if (users) {
      return JSON.parse(users);
    } else {
      return null;
    }
  }

  public createUserFormData(loggedInUsername: string, user: User, profileImage: File): FormData {
    const formData = new FormData();
    formData.append('currentUsername', loggedInUsername);
    formData.append('firstName', user.firstName);
    formData.append('lastName', user.lastName);
    formData.append('username', user.username);
    formData.append('email', user.email);
    formData.append('role', user.role);
    formData.append('profileImage', profileImage);
    formData.append('active', JSON.stringify(user.active));
    formData.append('notLocked', JSON.stringify(user.notLocked));
    return formData;
  }
}
