/*
Command used to generate:
ng g c pages/user --skip-tests --module app
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, Subscription} from "rxjs";
import {User} from "../../model/user";
import {UserService} from "../../services/user.service";
import {NotificationService} from "../../services/notification.service";
import {HttpErrorResponse, HttpEvent, HttpEventType} from "@angular/common/http";
import {NgForm} from "@angular/forms";
import {CustomHttpResponse} from "../../model/custom-http-response";
import {AuthenticationService} from "../../services/authentication.service";
import {Router} from "@angular/router";
import {FileUploadStatus} from "../../model/file-upload.status";
import {Role} from "../../enums/role.enum";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {

  private titleSubject = new BehaviorSubject<string>('Users');
  public titleAction$ = this.titleSubject.asObservable();
  public users: User[];
  public user: User;
  public selectedUser: User;
  public editUser: User = new User();
  public refreshing: boolean;
  private subscriptions: Subscription[] = [];
  public fileName: string;
  private profileImage: File;
  private originalUsername: string;
  public fileStatus: FileUploadStatus = new FileUploadStatus();

  constructor(private userService: UserService,
              private authenticationService: AuthenticationService,
              private router: Router,
              private notificationService: NotificationService) {
  }

  public changeTitle(title: string): void {
    this.titleSubject.next(title);
  }

  ngOnInit(): void {
    this.user = this.authenticationService.getUserFromLocalCache();
    this.getUsers(true);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  getUsers(showNotification: boolean) {
    this.refreshing = true;
    this.subscriptions.push(this.userService.getUsers().subscribe(
      (response: User[]) => {
        this.userService.addUsersToLocalCache(response);
        this.users = response;
        this.refreshing = false;
        if (showNotification) {
          this.notificationService.notifySuccess(`${response.length} user(s) loaded successfully`);
        }
      },
      (errorResponse: HttpErrorResponse) => {
        this.showError(errorResponse);
        this.refreshing = false;
      }
    ))
  }

  public searchUsers(searchTerm: string): void {
    if (!searchTerm) {
      this.users = this.userService.getUsersFromLocalCache();
      return;
    }
    const search = searchTerm.toLowerCase();
    const result: User[] = [];
    for (const user of this.userService.getUsersFromLocalCache()) {
      if (user.firstName.toLowerCase().indexOf(search) !== -1
          || user.lastName.toLowerCase().indexOf(search) !== -1
          || user.username.toLowerCase().indexOf(search) !== -1
          || user.email.toLowerCase().indexOf(search) !== -1
          || user.userId.toLowerCase().indexOf(search) !== -1) {
        result.push(user);
      }
    }
    this.users = result;
    if (result.length === 0) {
      this.users = this.userService.getUsersFromLocalCache();
    }
  }

  public onSelectUser(appUser: User): void {
    this.selectedUser = appUser;
    UserComponent.clickButtonById('openUserInfo');
  }

  public onProfileImageChange(event: Event) {
    const target = event.target;
    // @ts-ignore
    if (target.files.length === 1) {
      // @ts-ignore
      const file = target.files[0];
      this.fileName = file.name;
      this.profileImage = file;
    }
  }

  public saveNewUser(): void {
    document.getElementById('new-user-save').click();
  }

  public onAddNewUser(userForm: NgForm): void {
    const formData = this.userService.createUserFormData(null, userForm.value, this.profileImage);
    this.subscriptions.push(this.userService.addUser(formData).subscribe(
      (response: User) => {
        UserComponent.clickButtonById('new-user-close');
        this.getUsers(false);
        this.fileName = null;
        this.profileImage = null;
        userForm.reset();
        this.notificationService.notifySuccess(`${response.firstName} ${response.lastName} added successfully`);
      },
      (errorResponse: HttpErrorResponse) => {
        this.showError(errorResponse);
        this.fileName = null;
        this.profileImage = null;
      }
    ));
  }

  public onEditUser(editUser: User): void {
    this.editUser = editUser;
    this.originalUsername = editUser.username;
    UserComponent.clickButtonById('openUserEdit');
  }

  public onUpdateUser(): void {
    const formData = this.userService.createUserFormData(this.originalUsername, this.editUser, this.profileImage);
    this.subscriptions.push(this.userService.updateUser(formData).subscribe(
      (response: User) => {
        UserComponent.clickButtonById('closeEditUserModalButton');
        this.getUsers(false);
        this.fileName = null;
        this.profileImage = null;
        this.notificationService.notifySuccess(`${response.firstName} ${response.lastName} updated successfully`);
      },
      (errorResponse: HttpErrorResponse) => {
        this.showError(errorResponse);
        this.fileName = null;
        this.profileImage = null;
      }
    ));
  }

  public onDeleteUser(username: string): void {
    // todo: Add Confirmation Dialog
    this.subscriptions.push(this.userService.deleteUserByUsername(username).subscribe(
      (response: CustomHttpResponse) => {
        this.notificationService.notifySuccess(response.message);
        this.getUsers(false);
      },
      (errorResponse: HttpErrorResponse) => {
        this.showError(errorResponse);
      }
    ));
  }

  public onResetPassword(emailForm: NgForm): void {
    this.refreshing = true;
    const emailAddress = emailForm.value['reset-password-email'];
    this.subscriptions.push(this.userService.resetPasswordFor(emailAddress).subscribe(
      (response: CustomHttpResponse) => {
        this.notificationService.notifySuccess(response.message);
        this.refreshing = false;
      },
      (errorResponse: HttpErrorResponse) => {
        this.showError(errorResponse);
        this.refreshing = false;
      },
      () => emailForm.reset()
    ));
  }

  updateProfileImage() {
    UserComponent.clickButtonById('profile-image-input');
  }

  onUpdateProfileImage() {
    const formData = new FormData();
    formData.append('username', this.user.username);
    formData.append('profileImage', this.profileImage);
    this.subscriptions.push(this.userService.updateProfileImage(formData).subscribe(
      (event: HttpEvent<User>) => {
        this.reportUploadProgress(event);
      },
      (errorResponse: HttpErrorResponse) => {
        this.fileStatus.status = 'done';
        this.showError(errorResponse);
      }
    ));
  }

  private reportUploadProgress(event: HttpEvent<User>): void {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        this.fileStatus.percentage = Math.round(100 * event.loaded / event.total);
        this.fileStatus.status = 'progress';
        break;
      case HttpEventType.Response:
        if (event.status === 200) {
          this.user.profileImageUrl = `${event.body.profileImageUrl}?time=${new Date().getTime()}`;
          this.notificationService.notifySuccess(`${event.body.firstName}'s profile image updated successfully`);
          this.fileStatus.status = 'done';
        } else {
          this.notificationService.notifyError('Unable to upload image. Please try again');
        }
        break;
      default:
        `Finished all processes`;
    }
  }

  onUpdateCurrentUser(currentUser: User): void {
    this.refreshing = true;
    const formData = this.userService.createUserFormData(this.user.username, currentUser, this.profileImage);
    this.subscriptions.push(this.userService.updateUser(formData).subscribe(
      (response: User) => {
        this.authenticationService.addUserToLocalCache(response);
        this.getUsers(false);
        this.refreshing = false;
        this.fileName = null;
        this.profileImage = null;
        this.notificationService.notifySuccess(`${response.firstName} ${response.lastName} updated successfully`);
      },
      (errorResponse: HttpErrorResponse) => {
        this.refreshing = false;
        this.showError(errorResponse);
        this.fileName = null;
        this.profileImage = null;
      }
    ));
  }

  onLogOut() {
    this.authenticationService.logOut();
    this.notificationService.notifySuccess("You've been successfully logged out");
    this.router.navigate(['/login']);
  }

  public get isSuperAdmin(): boolean {
    return this.getUserRole() === Role.SUPER_ADMIN;
  }

  public get isAdmin(): boolean {
    const userRole = this.getUserRole();
    return userRole === Role.ADMIN || userRole === Role.SUPER_ADMIN;
  }

  public get isManager(): boolean {
    const userRole = this.getUserRole();
    return userRole === Role.ADMIN || userRole === Role.SUPER_ADMIN || userRole === Role.MANAGER;
  }

  public get isAdminOrManager(): boolean {
    const userRole = this.getUserRole();
    return userRole === Role.ADMIN || userRole === Role.SUPER_ADMIN || userRole === Role.MANAGER;
  }

  private getUserRole(): string {
    return this.authenticationService.getUserFromLocalCache().role;
  }

  private static clickButtonById(buttonId: string): void {
    document.getElementById(buttonId).click();
  }

  private showError(errorResponse: HttpErrorResponse): void {
    if (errorResponse.error.message) {
      this.notificationService.notifyError(errorResponse.error.message);
    } else {
      this.notificationService.notifyError('An error occurred. Please try again');
    }
  }
}
