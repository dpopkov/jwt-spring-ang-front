/*
Command used to generate:
ng g c pages/register --skip-tests --module app
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from "../../model/user";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../services/authentication.service";
import {NotificationService} from "../../services/notification.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {

  public showLoading: boolean;
  private readonly SUCCESS_URL = '/user/management';
  private subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private authService: AuthenticationService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl(this.SUCCESS_URL);
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  onRegister(user: User) {
    this.showLoading = true;
    this.subscriptions.push(
      this.authService.register(user).subscribe(
        (regUser: User) => {
          this.notificationService.notifySuccess(`A new account was created for ${regUser.username}.
          Pleas check your email for generated password to log in.`);
          this.showLoading = false;
          this.router.navigateByUrl('/login');
        },
        (errorResponse: HttpErrorResponse) => {
          this.showRegisterError(errorResponse);
          this.showLoading = false;
        }
      )
    );
  }

  private showRegisterError(errorResponse: HttpErrorResponse): void {
    if (errorResponse.error.message) {
      this.notificationService.notifyError(errorResponse.error.message);
    } else {
      this.notificationService.notifyError('An error occurred when registering. Please try again');
    }
  }
}
